#!/bin/bash
# mtpt is "mouse touchpad toggle"

# Verify input
# Catch blank argument 
if [ -z $1 ];
then
  echo "Error. Must specify 'touchpad' or 'mouse' argument."
  exit 1
# Catch invalid argument
elif [ $1 != "mouse" ] && [ $1 != "touchpad" ]
then
  echo "Error. Must specify 'touchpad' or 'mouse' argument."
  exit 1
fi

# Toggle mouse and touchpad configurations
if [ $1 = "touchpad" ];
then
  /usr/bin/killall imwheel
  /usr/bin/libinput-gestures-setup start
elif [ $1 = "mouse" ];
then
  /usr/bin/imwheel -b "4 5"
  /usr/bin/libinput-gestures-setup stop
fi